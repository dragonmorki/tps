// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "TPS/ProjectileDefault_Grenade.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeProjectileDefault_Grenade() {}
// Cross Module References
	TPS_API UClass* Z_Construct_UClass_AProjectileDefault_Grenade_NoRegister();
	TPS_API UClass* Z_Construct_UClass_AProjectileDefault_Grenade();
	TPS_API UClass* Z_Construct_UClass_AProjectileDefault();
	UPackage* Z_Construct_UPackage__Script_TPS();
// End Cross Module References
	void AProjectileDefault_Grenade::StaticRegisterNativesAProjectileDefault_Grenade()
	{
	}
	UClass* Z_Construct_UClass_AProjectileDefault_Grenade_NoRegister()
	{
		return AProjectileDefault_Grenade::StaticClass();
	}
	struct Z_Construct_UClass_AProjectileDefault_Grenade_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AProjectileDefault_Grenade_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AProjectileDefault,
		(UObject* (*)())Z_Construct_UPackage__Script_TPS,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AProjectileDefault_Grenade_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n *\n */" },
		{ "IncludePath", "ProjectileDefault_Grenade.h" },
		{ "ModuleRelativePath", "ProjectileDefault_Grenade.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_AProjectileDefault_Grenade_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AProjectileDefault_Grenade>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AProjectileDefault_Grenade_Statics::ClassParams = {
		&AProjectileDefault_Grenade::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_AProjectileDefault_Grenade_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AProjectileDefault_Grenade_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AProjectileDefault_Grenade()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AProjectileDefault_Grenade_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AProjectileDefault_Grenade, 3093517578);
	template<> TPS_API UClass* StaticClass<AProjectileDefault_Grenade>()
	{
		return AProjectileDefault_Grenade::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AProjectileDefault_Grenade(Z_Construct_UClass_AProjectileDefault_Grenade, &AProjectileDefault_Grenade::StaticClass, TEXT("/Script/TPS"), TEXT("AProjectileDefault_Grenade"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AProjectileDefault_Grenade);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
