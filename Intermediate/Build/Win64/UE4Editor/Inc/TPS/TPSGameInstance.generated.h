// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
struct FDropItem;
struct FWeaponInfo;
#ifdef TPS_TPSGameInstance_generated_h
#error "TPSGameInstance.generated.h already included, missing '#pragma once' in TPSGameInstance.h"
#endif
#define TPS_TPSGameInstance_generated_h

#define TPS_Source_TPS_TPSGameInstance_h_22_SPARSE_DATA
#define TPS_Source_TPS_TPSGameInstance_h_22_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGetDropItemInfoByName); \
	DECLARE_FUNCTION(execGetWeaponInfoByName);


#define TPS_Source_TPS_TPSGameInstance_h_22_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGetDropItemInfoByName); \
	DECLARE_FUNCTION(execGetWeaponInfoByName);


#define TPS_Source_TPS_TPSGameInstance_h_22_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUTPSGameInstance(); \
	friend struct Z_Construct_UClass_UTPSGameInstance_Statics; \
public: \
	DECLARE_CLASS(UTPSGameInstance, UGameInstance, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/TPS"), NO_API) \
	DECLARE_SERIALIZER(UTPSGameInstance)


#define TPS_Source_TPS_TPSGameInstance_h_22_INCLASS \
private: \
	static void StaticRegisterNativesUTPSGameInstance(); \
	friend struct Z_Construct_UClass_UTPSGameInstance_Statics; \
public: \
	DECLARE_CLASS(UTPSGameInstance, UGameInstance, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/TPS"), NO_API) \
	DECLARE_SERIALIZER(UTPSGameInstance)


#define TPS_Source_TPS_TPSGameInstance_h_22_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UTPSGameInstance(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UTPSGameInstance) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UTPSGameInstance); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UTPSGameInstance); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UTPSGameInstance(UTPSGameInstance&&); \
	NO_API UTPSGameInstance(const UTPSGameInstance&); \
public:


#define TPS_Source_TPS_TPSGameInstance_h_22_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UTPSGameInstance(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UTPSGameInstance(UTPSGameInstance&&); \
	NO_API UTPSGameInstance(const UTPSGameInstance&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UTPSGameInstance); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UTPSGameInstance); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UTPSGameInstance)


#define TPS_Source_TPS_TPSGameInstance_h_22_PRIVATE_PROPERTY_OFFSET
#define TPS_Source_TPS_TPSGameInstance_h_19_PROLOG
#define TPS_Source_TPS_TPSGameInstance_h_22_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TPS_Source_TPS_TPSGameInstance_h_22_PRIVATE_PROPERTY_OFFSET \
	TPS_Source_TPS_TPSGameInstance_h_22_SPARSE_DATA \
	TPS_Source_TPS_TPSGameInstance_h_22_RPC_WRAPPERS \
	TPS_Source_TPS_TPSGameInstance_h_22_INCLASS \
	TPS_Source_TPS_TPSGameInstance_h_22_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define TPS_Source_TPS_TPSGameInstance_h_22_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TPS_Source_TPS_TPSGameInstance_h_22_PRIVATE_PROPERTY_OFFSET \
	TPS_Source_TPS_TPSGameInstance_h_22_SPARSE_DATA \
	TPS_Source_TPS_TPSGameInstance_h_22_RPC_WRAPPERS_NO_PURE_DECLS \
	TPS_Source_TPS_TPSGameInstance_h_22_INCLASS_NO_PURE_DECLS \
	TPS_Source_TPS_TPSGameInstance_h_22_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> TPS_API UClass* StaticClass<class UTPSGameInstance>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID TPS_Source_TPS_TPSGameInstance_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
