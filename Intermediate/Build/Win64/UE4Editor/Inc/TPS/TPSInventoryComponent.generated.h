// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
struct FWeaponSlot;
enum class EWeaponType : uint8;
struct FAdditionalWeaponInfo;
struct FDropItem;
#ifdef TPS_TPSInventoryComponent_generated_h
#error "TPSInventoryComponent.generated.h already included, missing '#pragma once' in TPSInventoryComponent.h"
#endif
#define TPS_TPSInventoryComponent_generated_h

#define TPS_Source_TPS_TPSInventoryComponent_h_16_DELEGATE \
struct _Script_TPS_eventOnUpdateWeaponSlots_Parms \
{ \
	int32 IndexSlotChange; \
	FWeaponSlot NewInfo; \
}; \
static inline void FOnUpdateWeaponSlots_DelegateWrapper(const FMulticastScriptDelegate& OnUpdateWeaponSlots, int32 IndexSlotChange, FWeaponSlot NewInfo) \
{ \
	_Script_TPS_eventOnUpdateWeaponSlots_Parms Parms; \
	Parms.IndexSlotChange=IndexSlotChange; \
	Parms.NewInfo=NewInfo; \
	OnUpdateWeaponSlots.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define TPS_Source_TPS_TPSInventoryComponent_h_15_DELEGATE \
struct _Script_TPS_eventOnWeaponAmmoAviable_Parms \
{ \
	EWeaponType WeaponType; \
}; \
static inline void FOnWeaponAmmoAviable_DelegateWrapper(const FMulticastScriptDelegate& OnWeaponAmmoAviable, EWeaponType WeaponType) \
{ \
	_Script_TPS_eventOnWeaponAmmoAviable_Parms Parms; \
	Parms.WeaponType=WeaponType; \
	OnWeaponAmmoAviable.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define TPS_Source_TPS_TPSInventoryComponent_h_14_DELEGATE \
struct _Script_TPS_eventOnWeaponAmmoEmpty_Parms \
{ \
	EWeaponType WeaponType; \
}; \
static inline void FOnWeaponAmmoEmpty_DelegateWrapper(const FMulticastScriptDelegate& OnWeaponAmmoEmpty, EWeaponType WeaponType) \
{ \
	_Script_TPS_eventOnWeaponAmmoEmpty_Parms Parms; \
	Parms.WeaponType=WeaponType; \
	OnWeaponAmmoEmpty.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define TPS_Source_TPS_TPSInventoryComponent_h_13_DELEGATE \
struct _Script_TPS_eventOnWeaponAdditionalInfoChange_Parms \
{ \
	int32 IndexSlot; \
	FAdditionalWeaponInfo AdditionalInfo; \
}; \
static inline void FOnWeaponAdditionalInfoChange_DelegateWrapper(const FMulticastScriptDelegate& OnWeaponAdditionalInfoChange, int32 IndexSlot, FAdditionalWeaponInfo AdditionalInfo) \
{ \
	_Script_TPS_eventOnWeaponAdditionalInfoChange_Parms Parms; \
	Parms.IndexSlot=IndexSlot; \
	Parms.AdditionalInfo=AdditionalInfo; \
	OnWeaponAdditionalInfoChange.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define TPS_Source_TPS_TPSInventoryComponent_h_12_DELEGATE \
struct _Script_TPS_eventOnAmmoChange_Parms \
{ \
	EWeaponType TypeAmmo; \
	int32 Cout; \
}; \
static inline void FOnAmmoChange_DelegateWrapper(const FMulticastScriptDelegate& OnAmmoChange, EWeaponType TypeAmmo, int32 Cout) \
{ \
	_Script_TPS_eventOnAmmoChange_Parms Parms; \
	Parms.TypeAmmo=TypeAmmo; \
	Parms.Cout=Cout; \
	OnAmmoChange.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define TPS_Source_TPS_TPSInventoryComponent_h_11_DELEGATE \
struct _Script_TPS_eventOnSwitchWeapon_Parms \
{ \
	FName WeaponIdName; \
	FAdditionalWeaponInfo WeaponAdditionalInfo; \
	int32 NewCurrentIndexWeapon; \
}; \
static inline void FOnSwitchWeapon_DelegateWrapper(const FMulticastScriptDelegate& OnSwitchWeapon, FName WeaponIdName, FAdditionalWeaponInfo WeaponAdditionalInfo, int32 NewCurrentIndexWeapon) \
{ \
	_Script_TPS_eventOnSwitchWeapon_Parms Parms; \
	Parms.WeaponIdName=WeaponIdName; \
	Parms.WeaponAdditionalInfo=WeaponAdditionalInfo; \
	Parms.NewCurrentIndexWeapon=NewCurrentIndexWeapon; \
	OnSwitchWeapon.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define TPS_Source_TPS_TPSInventoryComponent_h_21_SPARSE_DATA
#define TPS_Source_TPS_TPSInventoryComponent_h_21_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGetDropItemInfoFromInventory); \
	DECLARE_FUNCTION(execTryGetWeaponToInventory); \
	DECLARE_FUNCTION(execSwitchWeaponToInventory); \
	DECLARE_FUNCTION(execCheckCanTakeWeapon); \
	DECLARE_FUNCTION(execCheckCanTakeAmmo); \
	DECLARE_FUNCTION(execAmmoSlotChangeValue);


#define TPS_Source_TPS_TPSInventoryComponent_h_21_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGetDropItemInfoFromInventory); \
	DECLARE_FUNCTION(execTryGetWeaponToInventory); \
	DECLARE_FUNCTION(execSwitchWeaponToInventory); \
	DECLARE_FUNCTION(execCheckCanTakeWeapon); \
	DECLARE_FUNCTION(execCheckCanTakeAmmo); \
	DECLARE_FUNCTION(execAmmoSlotChangeValue);


#define TPS_Source_TPS_TPSInventoryComponent_h_21_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUTPSInventoryComponent(); \
	friend struct Z_Construct_UClass_UTPSInventoryComponent_Statics; \
public: \
	DECLARE_CLASS(UTPSInventoryComponent, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/TPS"), NO_API) \
	DECLARE_SERIALIZER(UTPSInventoryComponent)


#define TPS_Source_TPS_TPSInventoryComponent_h_21_INCLASS \
private: \
	static void StaticRegisterNativesUTPSInventoryComponent(); \
	friend struct Z_Construct_UClass_UTPSInventoryComponent_Statics; \
public: \
	DECLARE_CLASS(UTPSInventoryComponent, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/TPS"), NO_API) \
	DECLARE_SERIALIZER(UTPSInventoryComponent)


#define TPS_Source_TPS_TPSInventoryComponent_h_21_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UTPSInventoryComponent(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UTPSInventoryComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UTPSInventoryComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UTPSInventoryComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UTPSInventoryComponent(UTPSInventoryComponent&&); \
	NO_API UTPSInventoryComponent(const UTPSInventoryComponent&); \
public:


#define TPS_Source_TPS_TPSInventoryComponent_h_21_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UTPSInventoryComponent(UTPSInventoryComponent&&); \
	NO_API UTPSInventoryComponent(const UTPSInventoryComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UTPSInventoryComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UTPSInventoryComponent); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UTPSInventoryComponent)


#define TPS_Source_TPS_TPSInventoryComponent_h_21_PRIVATE_PROPERTY_OFFSET
#define TPS_Source_TPS_TPSInventoryComponent_h_18_PROLOG
#define TPS_Source_TPS_TPSInventoryComponent_h_21_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TPS_Source_TPS_TPSInventoryComponent_h_21_PRIVATE_PROPERTY_OFFSET \
	TPS_Source_TPS_TPSInventoryComponent_h_21_SPARSE_DATA \
	TPS_Source_TPS_TPSInventoryComponent_h_21_RPC_WRAPPERS \
	TPS_Source_TPS_TPSInventoryComponent_h_21_INCLASS \
	TPS_Source_TPS_TPSInventoryComponent_h_21_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define TPS_Source_TPS_TPSInventoryComponent_h_21_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TPS_Source_TPS_TPSInventoryComponent_h_21_PRIVATE_PROPERTY_OFFSET \
	TPS_Source_TPS_TPSInventoryComponent_h_21_SPARSE_DATA \
	TPS_Source_TPS_TPSInventoryComponent_h_21_RPC_WRAPPERS_NO_PURE_DECLS \
	TPS_Source_TPS_TPSInventoryComponent_h_21_INCLASS_NO_PURE_DECLS \
	TPS_Source_TPS_TPSInventoryComponent_h_21_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> TPS_API UClass* StaticClass<class UTPSInventoryComponent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID TPS_Source_TPS_TPSInventoryComponent_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
