// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UDecalComponent;
class UAnimMontage;
struct FAdditionalWeaponInfo;
class AWeaponDefault;
#ifdef TPS_TPSCharacter_generated_h
#error "TPSCharacter.generated.h already included, missing '#pragma once' in TPSCharacter.h"
#endif
#define TPS_TPSCharacter_generated_h

#define TPS_Source_TPS_TPSCharacter_h_24_SPARSE_DATA
#define TPS_Source_TPS_TPSCharacter_h_24_RPC_WRAPPERS \
	virtual void WeaponFireStart_BP_Implementation(UAnimMontage* Anim); \
	virtual void WeaponReloadEnd_BP_Implementation(bool bIsSuccess); \
	virtual void WeaponReloadStart_BP_Implementation(UAnimMontage* Anim); \
 \
	DECLARE_FUNCTION(execGetCursorToWorld); \
	DECLARE_FUNCTION(execWeaponFireStart_BP); \
	DECLARE_FUNCTION(execWeaponFireStart); \
	DECLARE_FUNCTION(execWeaponReloadEnd_BP); \
	DECLARE_FUNCTION(execWeaponReloadStart_BP); \
	DECLARE_FUNCTION(execWeaponReloadEnd); \
	DECLARE_FUNCTION(execWeaponReloadStart); \
	DECLARE_FUNCTION(execTryReloadWeapon); \
	DECLARE_FUNCTION(execRemoveCurrentWeapon); \
	DECLARE_FUNCTION(execInitWeapon); \
	DECLARE_FUNCTION(execGetCurrentWeapon); \
	DECLARE_FUNCTION(execChangeMovementState); \
	DECLARE_FUNCTION(execCharacterUpdate); \
	DECLARE_FUNCTION(execAttackCharEvent); \
	DECLARE_FUNCTION(execMovementTick); \
	DECLARE_FUNCTION(execInputAttackReleased); \
	DECLARE_FUNCTION(execInputAttackPressed); \
	DECLARE_FUNCTION(execInputAxisX); \
	DECLARE_FUNCTION(execInputAxisY);


#define TPS_Source_TPS_TPSCharacter_h_24_RPC_WRAPPERS_NO_PURE_DECLS \
	virtual void WeaponFireStart_BP_Implementation(UAnimMontage* Anim); \
	virtual void WeaponReloadEnd_BP_Implementation(bool bIsSuccess); \
	virtual void WeaponReloadStart_BP_Implementation(UAnimMontage* Anim); \
 \
	DECLARE_FUNCTION(execGetCursorToWorld); \
	DECLARE_FUNCTION(execWeaponFireStart_BP); \
	DECLARE_FUNCTION(execWeaponFireStart); \
	DECLARE_FUNCTION(execWeaponReloadEnd_BP); \
	DECLARE_FUNCTION(execWeaponReloadStart_BP); \
	DECLARE_FUNCTION(execWeaponReloadEnd); \
	DECLARE_FUNCTION(execWeaponReloadStart); \
	DECLARE_FUNCTION(execTryReloadWeapon); \
	DECLARE_FUNCTION(execRemoveCurrentWeapon); \
	DECLARE_FUNCTION(execInitWeapon); \
	DECLARE_FUNCTION(execGetCurrentWeapon); \
	DECLARE_FUNCTION(execChangeMovementState); \
	DECLARE_FUNCTION(execCharacterUpdate); \
	DECLARE_FUNCTION(execAttackCharEvent); \
	DECLARE_FUNCTION(execMovementTick); \
	DECLARE_FUNCTION(execInputAttackReleased); \
	DECLARE_FUNCTION(execInputAttackPressed); \
	DECLARE_FUNCTION(execInputAxisX); \
	DECLARE_FUNCTION(execInputAxisY);


#define TPS_Source_TPS_TPSCharacter_h_24_EVENT_PARMS \
	struct TPSCharacter_eventWeaponFireStart_BP_Parms \
	{ \
		UAnimMontage* Anim; \
	}; \
	struct TPSCharacter_eventWeaponReloadEnd_BP_Parms \
	{ \
		bool bIsSuccess; \
	}; \
	struct TPSCharacter_eventWeaponReloadStart_BP_Parms \
	{ \
		UAnimMontage* Anim; \
	};


#define TPS_Source_TPS_TPSCharacter_h_24_CALLBACK_WRAPPERS
#define TPS_Source_TPS_TPSCharacter_h_24_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesATPSCharacter(); \
	friend struct Z_Construct_UClass_ATPSCharacter_Statics; \
public: \
	DECLARE_CLASS(ATPSCharacter, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/TPS"), NO_API) \
	DECLARE_SERIALIZER(ATPSCharacter)


#define TPS_Source_TPS_TPSCharacter_h_24_INCLASS \
private: \
	static void StaticRegisterNativesATPSCharacter(); \
	friend struct Z_Construct_UClass_ATPSCharacter_Statics; \
public: \
	DECLARE_CLASS(ATPSCharacter, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/TPS"), NO_API) \
	DECLARE_SERIALIZER(ATPSCharacter)


#define TPS_Source_TPS_TPSCharacter_h_24_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ATPSCharacter(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ATPSCharacter) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ATPSCharacter); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ATPSCharacter); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ATPSCharacter(ATPSCharacter&&); \
	NO_API ATPSCharacter(const ATPSCharacter&); \
public:


#define TPS_Source_TPS_TPSCharacter_h_24_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ATPSCharacter(ATPSCharacter&&); \
	NO_API ATPSCharacter(const ATPSCharacter&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ATPSCharacter); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ATPSCharacter); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ATPSCharacter)


#define TPS_Source_TPS_TPSCharacter_h_24_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__TopDownCameraComponent() { return STRUCT_OFFSET(ATPSCharacter, TopDownCameraComponent); } \
	FORCEINLINE static uint32 __PPO__CameraBoom() { return STRUCT_OFFSET(ATPSCharacter, CameraBoom); }


#define TPS_Source_TPS_TPSCharacter_h_21_PROLOG \
	TPS_Source_TPS_TPSCharacter_h_24_EVENT_PARMS


#define TPS_Source_TPS_TPSCharacter_h_24_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TPS_Source_TPS_TPSCharacter_h_24_PRIVATE_PROPERTY_OFFSET \
	TPS_Source_TPS_TPSCharacter_h_24_SPARSE_DATA \
	TPS_Source_TPS_TPSCharacter_h_24_RPC_WRAPPERS \
	TPS_Source_TPS_TPSCharacter_h_24_CALLBACK_WRAPPERS \
	TPS_Source_TPS_TPSCharacter_h_24_INCLASS \
	TPS_Source_TPS_TPSCharacter_h_24_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define TPS_Source_TPS_TPSCharacter_h_24_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TPS_Source_TPS_TPSCharacter_h_24_PRIVATE_PROPERTY_OFFSET \
	TPS_Source_TPS_TPSCharacter_h_24_SPARSE_DATA \
	TPS_Source_TPS_TPSCharacter_h_24_RPC_WRAPPERS_NO_PURE_DECLS \
	TPS_Source_TPS_TPSCharacter_h_24_CALLBACK_WRAPPERS \
	TPS_Source_TPS_TPSCharacter_h_24_INCLASS_NO_PURE_DECLS \
	TPS_Source_TPS_TPSCharacter_h_24_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> TPS_API UClass* StaticClass<class ATPSCharacter>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID TPS_Source_TPS_TPSCharacter_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
