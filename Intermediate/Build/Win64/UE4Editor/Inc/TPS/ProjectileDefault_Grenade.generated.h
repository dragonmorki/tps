// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef TPS_ProjectileDefault_Grenade_generated_h
#error "ProjectileDefault_Grenade.generated.h already included, missing '#pragma once' in ProjectileDefault_Grenade.h"
#endif
#define TPS_ProjectileDefault_Grenade_generated_h

#define TPS_Source_TPS_ProjectileDefault_Grenade_h_15_SPARSE_DATA
#define TPS_Source_TPS_ProjectileDefault_Grenade_h_15_RPC_WRAPPERS
#define TPS_Source_TPS_ProjectileDefault_Grenade_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define TPS_Source_TPS_ProjectileDefault_Grenade_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAProjectileDefault_Grenade(); \
	friend struct Z_Construct_UClass_AProjectileDefault_Grenade_Statics; \
public: \
	DECLARE_CLASS(AProjectileDefault_Grenade, AProjectileDefault, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/TPS"), NO_API) \
	DECLARE_SERIALIZER(AProjectileDefault_Grenade)


#define TPS_Source_TPS_ProjectileDefault_Grenade_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAProjectileDefault_Grenade(); \
	friend struct Z_Construct_UClass_AProjectileDefault_Grenade_Statics; \
public: \
	DECLARE_CLASS(AProjectileDefault_Grenade, AProjectileDefault, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/TPS"), NO_API) \
	DECLARE_SERIALIZER(AProjectileDefault_Grenade)


#define TPS_Source_TPS_ProjectileDefault_Grenade_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AProjectileDefault_Grenade(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AProjectileDefault_Grenade) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AProjectileDefault_Grenade); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AProjectileDefault_Grenade); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AProjectileDefault_Grenade(AProjectileDefault_Grenade&&); \
	NO_API AProjectileDefault_Grenade(const AProjectileDefault_Grenade&); \
public:


#define TPS_Source_TPS_ProjectileDefault_Grenade_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AProjectileDefault_Grenade() { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AProjectileDefault_Grenade(AProjectileDefault_Grenade&&); \
	NO_API AProjectileDefault_Grenade(const AProjectileDefault_Grenade&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AProjectileDefault_Grenade); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AProjectileDefault_Grenade); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AProjectileDefault_Grenade)


#define TPS_Source_TPS_ProjectileDefault_Grenade_h_15_PRIVATE_PROPERTY_OFFSET
#define TPS_Source_TPS_ProjectileDefault_Grenade_h_12_PROLOG
#define TPS_Source_TPS_ProjectileDefault_Grenade_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TPS_Source_TPS_ProjectileDefault_Grenade_h_15_PRIVATE_PROPERTY_OFFSET \
	TPS_Source_TPS_ProjectileDefault_Grenade_h_15_SPARSE_DATA \
	TPS_Source_TPS_ProjectileDefault_Grenade_h_15_RPC_WRAPPERS \
	TPS_Source_TPS_ProjectileDefault_Grenade_h_15_INCLASS \
	TPS_Source_TPS_ProjectileDefault_Grenade_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define TPS_Source_TPS_ProjectileDefault_Grenade_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TPS_Source_TPS_ProjectileDefault_Grenade_h_15_PRIVATE_PROPERTY_OFFSET \
	TPS_Source_TPS_ProjectileDefault_Grenade_h_15_SPARSE_DATA \
	TPS_Source_TPS_ProjectileDefault_Grenade_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	TPS_Source_TPS_ProjectileDefault_Grenade_h_15_INCLASS_NO_PURE_DECLS \
	TPS_Source_TPS_ProjectileDefault_Grenade_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> TPS_API UClass* StaticClass<class AProjectileDefault_Grenade>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID TPS_Source_TPS_ProjectileDefault_Grenade_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
