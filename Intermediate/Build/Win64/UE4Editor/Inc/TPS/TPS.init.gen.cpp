// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeTPS_init() {}
	TPS_API UFunction* Z_Construct_UDelegateFunction_TPS_OnWeaponFireStart__DelegateSignature();
	TPS_API UFunction* Z_Construct_UDelegateFunction_TPS_OnWeaponReloadStart__DelegateSignature();
	TPS_API UFunction* Z_Construct_UDelegateFunction_TPS_OnWeaponReloadEnd__DelegateSignature();
	TPS_API UFunction* Z_Construct_UDelegateFunction_TPS_OnSwitchWeapon__DelegateSignature();
	TPS_API UFunction* Z_Construct_UDelegateFunction_TPS_OnAmmoChange__DelegateSignature();
	TPS_API UFunction* Z_Construct_UDelegateFunction_TPS_OnWeaponAdditionalInfoChange__DelegateSignature();
	TPS_API UFunction* Z_Construct_UDelegateFunction_TPS_OnWeaponAmmoEmpty__DelegateSignature();
	TPS_API UFunction* Z_Construct_UDelegateFunction_TPS_OnWeaponAmmoAviable__DelegateSignature();
	TPS_API UFunction* Z_Construct_UDelegateFunction_TPS_OnUpdateWeaponSlots__DelegateSignature();
	UPackage* Z_Construct_UPackage__Script_TPS()
	{
		static UPackage* ReturnPackage = nullptr;
		if (!ReturnPackage)
		{
			static UObject* (*const SingletonFuncArray[])() = {
				(UObject* (*)())Z_Construct_UDelegateFunction_TPS_OnWeaponFireStart__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_TPS_OnWeaponReloadStart__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_TPS_OnWeaponReloadEnd__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_TPS_OnSwitchWeapon__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_TPS_OnAmmoChange__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_TPS_OnWeaponAdditionalInfoChange__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_TPS_OnWeaponAmmoEmpty__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_TPS_OnWeaponAmmoAviable__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_TPS_OnUpdateWeaponSlots__DelegateSignature,
			};
			static const UE4CodeGen_Private::FPackageParams PackageParams = {
				"/Script/TPS",
				SingletonFuncArray,
				UE_ARRAY_COUNT(SingletonFuncArray),
				PKG_CompiledIn | 0x00000000,
				0xA1B60B61,
				0xFF1195C8,
				METADATA_PARAMS(nullptr, 0)
			};
			UE4CodeGen_Private::ConstructUPackage(ReturnPackage, PackageParams);
		}
		return ReturnPackage;
	}
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
